#!/bin/python3

# Imports
import sqlite3
import argparse
import sys

# Setup ArgParse
class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

parser = MyParser(
    prog="Test SQLite3", description="This is my test sqlite3 playground"
)

parser.add_argument("-t", "--task", metavar="TASK", help="Task name")
parser.add_argument("-d", "--desc", help="Task description", metavar="DESC", default="")
parser.add_argument("--read", action="store_true", help="Reads all tasks")
parser.add_argument("--remove", nargs='?', default="False", metavar="NUM", help="Remove NUM. NUM is optional")

args = parser.parse_args()

task_arg = args.task
desc_arg = args.desc
read_only = args.read
remove_item = args.remove

if len(sys.argv)==1:
    parser.print_help(sys.stderr)
    sys.exit(1)

# Setup database
conn = sqlite3.connect("Dans-Tasks.sqlite3")
curs = conn.cursor()


# Checks to see of the tasks table exists. Create it if it doesn't.
def create_table():
    listOfTables = curs.execute(
        """SELECT * FROM sqlite_master
        WHERE type='table'; """
    ).fetchall()

    if listOfTables == []:
        print("Table not found")
        curs.execute(
            """CREATE TABLE tasks(
            task_id INTEGER PRIMARY KEY AUTOINCREMENT ,
            task_name text,
            task_desc text)"""
        )
        print("Table created")
    else:
        return ()


def commit():
    # print("Commit and close connection.")
    conn.commit()
    conn.close()


def insert_task(task_name, task_desc):
    data = [task_name, task_desc]
    curs.execute(
        """INSERT INTO tasks
        (task_name, task_desc)
        VALUES (?, ?)""",
        data,
    )
    print("'" + task_name + "' added")


def fetch_and_print():
    print("ID" + "\t" + "Tasks" + "\t\t" + "Description")
    curs.execute("SELECT * FROM tasks;")
    rows = curs.fetchall()
    for row in rows:
        # The extra str bit it in case of empty tasks or descriptions
        print(str(row[0]) + "\t" + str(row[1]) + "\t" + str(row[2]))

def removeItem(num_to_remove):

    if num_to_remove == None:
        num_to_remove = input("What number would you like to remove? : ")

    try :
        num_to_remove == type(int(num_to_remove))
        print( "Removing " + str(num_to_remove))
        curs.execute("DELETE FROM tasks WHERE task_id is ? ; ", num_to_remove )

    except:
        print("Number to remove not a number")
        return()


# Main
if __name__ == "__main__":
    create_table()

    if read_only == True:
        fetch_and_print()
    elif remove_item != True or remove_item == str:
        removeItem(remove_item)
    elif task_arg :
        insert_task(task_arg, desc_arg)

    commit()

